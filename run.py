import argparse
import logging
import json
import os
import time

import cv2
import numpy as np

from ellcv.utils import pose_error, compute_ellipses_iou
from ellcv.visu import draw_ellipse, draw_bbox, draw_ellipse_dashed

from config.config import _cfg
from dataset.dataset_loader import Dataset_loader
from dataset.scene_loader import Scene_loader
from object_detector import Improved_object_detector, External_improved_object_detector
from utils.pose_computation import compute_pose
from utils.utils import create_if_needed

from utils.pose_refinement import PoseRefinement
from scipy.spatial.transform.rotation import Rotation as Rot

def to_json_serializable(detections):
    return [{"category_id": d["category_id"],
             "detection_score": d["detection_score"],
             "bbox": d["bbox"],
             "ellipses" : [e.to_dict() for e in d["ellipses"]]
            } for d in detections]


def save_obj(filename, points):
    v = np.hstack((np.repeat(np.array([["v"]]), points.shape[0], axis=0), points.astype(str)))
    np.savetxt(filename, v, fmt="%s")


def main(args):
    parser = argparse.ArgumentParser()
    parser.add_argument("scene", help="<Required> Input Scene file containing the objects (.json).")
    parser.add_argument("dataset", help="<Required> Dataset to process (.json).")
    parser.add_argument("detector_checkpoint", help="<Required> Checkpoint for the detection network.")
    parser.add_argument("ellipses_checkpoints", help="<Required> Folder containing the checkpoints for ellipse predictions")


    parser.add_argument("--external_detections", default=None,
                        help="<Optional> External file containing the detections (bbox and ellipses) for the dataset (default is None).")
    parser.add_argument("--mode", default="best",
                        help="Choose between 'best'(default) or 'last' checkpoint for ellipse prediction")
    parser.add_argument("--output_predictions", default=None,
                        help="<Optional> Output file containing the detected objects and predicted ellipses")
    parser.add_argument("--output_images", default=None,
                        help="<Optional> Output folder where to write the images with predictions and reporjected objects (default is None).")
    parser.add_argument("--output_errors", default=None,
                        help="<Optional> Output folder where to write the position and orientation errors (default is None).")
    parser.add_argument("--visualize",  action="store_true", default=False,
                        help="<Optional> Visualize each predictions and estimated poses online (default is False)")
    parser.add_argument("--only_prediction",  action="store_true", default=False,
                        help="<Optional> Do only object detection and ellipse prediction"
                            "without computing the camera pose (defaut is False).")
    parser.add_argument("--skip_frames", default=0, type=int,
                        help="<Optional> Skip frames to process (default is 0, no skipping).")
    parser.add_argument('--min_obj_for_P3P', choices=[3, 4], type=int, default=3,
                        help="<Optional> Minimum number of required detected objects to use P3P."
                             "When less (but >= 2) P2E can be used. (default is 3)")
    parser.add_argument("--disable_ellipses_prediction",  action="store_true", default=False,
                        help="<Optional> Disable ellipses prediction and fit ellipses to bounding boxes instead. "
                             "Just pass a random name for elllipses_checkpoints (default is False)")
    parser.add_argument("--refine_pose",  action="store_true", default=False,
                        help="<Optional> Enable pose refinement (default is False).")
    parser.add_argument("--THRESHOLD", default=0.2, type=float,
                        help="<Optional> Threshold used to determine between inlier and outlier (default is 0.2).")
    parser.add_argument("--loss", default="linear", type=str,
                        help="<Optional> Optional loss for least-square optimization (default is 'linear').")
    parser.add_argument("--f_scale", default=1.0, type=float,
                        help="<Optional> Scale factor used in the loss is enabled (default is 1.0).")
    parser.add_argument("--cost", default="level_set", type=str,
                        help="<Optional> Select the cost used in refinement (default is 'level_set').")
    parser.add_argument("--solver", default="bfgs", type=str,
                        help="<Optional> Select the solver to use for refinement ('lstsq', 'bfgs') (default is 'bfgs').")
    parser.add_argument("--optim_mapping",  action="store_true", default=False,
                        help="<Optional> Enable usage of uncertainty to choose one prediction (default is False).")
    parser.add_argument("--nb_sampling_pts",type=int, default=[24, 6], nargs=2,
                        help="<Optional> Number of sampling in azimtuh and along the radius (default is [24, 6])")
    parser.add_argument("--use_uncertainty",  action="store_true", default=False,
                        help="<Optional> Enable usage of uncertainty (default is False).")
    args = parser.parse_args(args)


    scene_file = args.scene
    dataset_file = args.dataset
    detector_checkpoint = args.detector_checkpoint
    checkpoints_folder_ellipses = args.ellipses_checkpoints
    mode = args.mode
    output_file = args.output_predictions
    output_folder = args.output_images
    output_errors = args.output_errors
    visualize = args.visualize
    skip_frames = args.skip_frames
    do_pose_computation = not args.only_prediction
    min_obj_for_P3P = args.min_obj_for_P3P
    enable_ellipses_prediction = not args.disable_ellipses_prediction
    refine_pose = args.refine_pose
    external_detections = args.external_detections
    THRESHOLD = args.THRESHOLD
    loss = args.loss
    f_scale = args.f_scale
    cost_mode = args.cost.lower()
    solver = args.solver
    optim_mapping = args.optim_mapping
    use_uncertainty = args.use_uncertainty
    N_az, N_dist = args.nb_sampling_pts[0], args.nb_sampling_pts[1]

    print("Refine camera pose: ", refine_pose)
    if refine_pose:
        print("metric: ", cost_mode)
        print("solver: ", solver)
        print("threshold ", THRESHOLD)
        if "sampling" in cost_mode:
            print("N_az: %d, N_dist: %d" % (N_az, N_dist))



    np.random.seed(1994)

    if output_folder is not None:
        create_if_needed(output_folder)
    if do_pose_computation and output_errors:
        create_if_needed(output_errors)

    # Load the scene
    scene = Scene_loader(scene_file)

    # Load the dataset
    loader = Dataset_loader(dataset_file)


    # Create the detector
    if external_detections is not None:
        detector = External_improved_object_detector(external_detections, enable_ellipses_prediction=enable_ellipses_prediction)
    else:
        detector = Improved_object_detector(detector_checkpoint, checkpoints_folder_ellipses, enable_ellipses_prediction=enable_ellipses_prediction, mode=mode, use_uncertainty=use_uncertainty)

    # Processing loop
    output_data = []
    position_errors = [-1] * len(loader)
    orientation_errors = [-1] * len(loader)
    nb_inliers = [-1] * len(loader)
    total_ious = [-1] * len(loader)
    times = [-1] * len(loader)
    ref_times = [-1] * len(loader)
    nb_iterations = [-1] * len(loader)
    out_gt_positions = np.zeros((len(loader), 3), dtype=float)
    out_est_positions = np.zeros((len(loader), 3), dtype=float)
    for idx in range(0, len(loader), 1+skip_frames):
        print("({:4} / {}) => ".format(idx, len(loader)), end=" ")
        f = loader.get_rgb_filename(idx)
        K = loader.get_K(idx)
        width, height = loader.get_image_size(idx)

        t_start = time.time()

        # Run detector
        detections = detector.detect(f)


        # Force instance association using uncertainty
        if optim_mapping:
            for i in range(len(detections)):
                best_index = np.argmin(detections[i]["uncertainties"])
                multi = len(detections[i]["uncertainties"]) > 1
                ell = detections[i]["ellipses"][best_index]
                unc = detections[i]["uncertainties"][best_index]
                obj_id = detections[i]["obj_ids"][best_index]
                detections[i]["ellipses"] = [ell]
                detections[i]["uncertainties"] = [unc]
                detections[i]["obj_ids"] = [obj_id]
                detections[i]["multi_predictions"] = multi

        out_data = {"file_name": f, 
                    "detections": to_json_serializable(detections),
                    "orientation": [],
                    "position": []}

        # # Filter detections based on detection score
        # filtered_detections = []
        # for d in detections:
        #     if d["detection_score"] >= 0.3:
        #         filtered_detections.append(d)
        # detections = filtered_detections

        if do_pose_computation:
            # Compute camera pose
            pose, used_pairs, inliers = compute_pose(detections, scene, K, min_obj_for_P3P=min_obj_for_P3P, THRESHOLD=THRESHOLD, image=cv2.imread(f))
            if pose is not None:
                o = pose[:3, :3]
                p = pose[:, 3]
                P_init = K @ np.hstack((o.T, (-o.T @ p).reshape((-3, 1))))
                nb_inliers[idx] = len(inliers)

                # Refinement
                if refine_pose:
                    inliers_ellipses = []
                    inliers_ellipsoids = []
                    inliers_uncertainties = [] if use_uncertainty else None
                    for inlier in inliers:
                        ell = detections[inlier[0][0]]["ellipses"][inlier[0][1]]
                        ellipsoid = scene[inlier[1]]["ellipsoid"]
                        inliers_ellipses.append(ell)
                        inliers_ellipsoids.append(ellipsoid)
                        if use_uncertainty:
                            inliers_uncertainties.append(detections[inlier[0][0]]["uncertainties"][inlier[0][1]])
                    pose_refiner = PoseRefinement(inliers_ellipses, inliers_ellipsoids, K, width, height, [o, p], cost_mode, solver=solver, loss=loss, f_scale=f_scale, uncertainties=inliers_uncertainties)
                    t_ref_start = time.time()
                    refined_orientation, refined_position, nb_iter = pose_refiner.optimize(display=False, img=None, name="", N_az=N_az, N_dist=N_dist)
                    ref_times[idx] = time.time() - t_ref_start
                    nb_iterations[idx] = nb_iter
                    o = refined_orientation
                    p = refined_position

                P = K @ np.hstack((o.T, (-o.T @ p).reshape((-3, 1))))
                times[idx] = time.time() - t_start


                # Evaluate pose error
                Rt_gt = loader.get_Rt(idx)
                orientation_gt = Rt_gt[:3, :3].T
                position_gt = -Rt_gt[:3, :3].T @ Rt_gt[:, 3]
                rot_error, pos_error = pose_error((o, p), (orientation_gt, position_gt))
                print("Estimated pose error: %.3fm %.2f°" % (pos_error, np.rad2deg(rot_error)), end=" ")
                print("in %.4fs" % times[idx])
                # save for output
                out_gt_positions[idx, :] = position_gt
                out_est_positions[idx, :] = p
                
                out_data["orientation"] = o.tolist()
                out_data["position"] = p.tolist()
                position_errors[idx] = pos_error
                orientation_errors[idx] = np.rad2deg(rot_error)

        output_data.append(out_data)
        


        if visualize or output_folder is not None:
            try:
                img = cv2.imread(f)
                if do_pose_computation and pose is not None:
                    # Display objects detections, ellipses predictions and reprojected
                    # ellipsoids using the estimated pose
                    used_ellipses = set([p[0] for p in used_pairs])
                    used_ellipsoids = set([p[1] for p in used_pairs])
                    inlier_ellipses = set([p[0] for p in inliers])
                    inlier_ellipsoids = set([p[1] for p in inliers])
                    for det_i, det in enumerate(detections):
                        bb = np.round(det["bbox"]).astype(int)
                        draw_bbox(img, bb, color=(255, 255, 255))
                        for pred_i, ell in enumerate(det["ellipses"]):
                            pred = (det_i, pred_i)
                            if pred in used_ellipses:
                                col = (0, 255, 0)
                            elif pred in inlier_ellipses:
                                col = (255, 100, 10)
                            else:
                                continue
                            draw_ellipse(img, ell, col, 2)
                        # cv2.putText(img, "det %d" % det_i, (bb[0], bb[1] + 20), cv2.FONT_HERSHEY_SIMPLEX , 0.5, col, 2, cv2.LINE_AA)

                    for obj_i, obj in enumerate(scene):
                        proj_ell = obj["ellipsoid"].project(P)
                        if obj_i in used_ellipsoids:
                            col = (0, 255, 0)
                        elif obj_i in inlier_ellipsoids:
                            col = (255, 100, 10)
                        else:
                            col = (0, 0, 255)
                        draw_ellipse_dashed(img, proj_ell, col, 4)
                        cent = proj_ell.center
                        # cv2.putText(img, str(obj["category_id"]), (int(cent[0]), int(cent[1])), cv2.FONT_HERSHEY_SIMPLEX , 1, col, 2, cv2.LINE_AA)
                        # cv2.putText(img, str(obj["object_id"]), (int(cent[0]), int(cent[1])+20), cv2.FONT_HERSHEY_SIMPLEX , 1, col, 2, cv2.LINE_AA)
                    if use_uncertainty:
                        for det_i, det in enumerate(detections):
                            for pred_i, ell in enumerate(det["ellipses"]):
                                pred = (det_i, pred_i)
                                if pred in used_ellipses or pred in inlier_ellipses:
                                    cv2.putText(img, "%.2f" % np.reciprocal(det["uncertainties"][pred_i]), (int(ell.center[0]-20), int(ell.center[1]+0*pred_i*30)), cv2.FONT_HERSHEY_SIMPLEX , 0.8, (0, 145, 255), 2, cv2.LINE_AA)
                    # cv2.putText(img, "%.3fm" % pos_error, (25, 30), cv2.FONT_HERSHEY_SIMPLEX , 1, (50, 255, 50), 2, cv2.LINE_AA)
                    # cv2.putText(img, "%.2fdeg" % np.rad2deg(rot_error), (475, 30), cv2.FONT_HERSHEY_SIMPLEX , 1, (50, 255, 50), 2, cv2.LINE_AA)
                    cv2.putText(img, "%.3fm" % pos_error, (200, 480-15), cv2.FONT_HERSHEY_SIMPLEX , 1, (50, 255, 50), 2, cv2.LINE_AA)
                    cv2.putText(img, "%.2fdeg" % np.rad2deg(rot_error), (350, 480-15), cv2.FONT_HERSHEY_SIMPLEX , 1, (50, 255, 50), 2, cv2.LINE_AA)
                else:
                    # Display only detections and predicted ellipses
                    colors = {}
                    for det_i, det in enumerate(detections):
                        bb = np.round(det["bbox"]).astype(int)
                        cat = det["category_id"]
                        if cat not in colors.keys():
                            colors[cat] = np.random.uniform(0, 255, size=(3))
                        draw_bbox(img, bb, color=colors[cat], thickness=2)
                        cv2.putText(img, "%.2f(%d)" % (det["detection_score"], det["category_id"]), (int(bb[0]+10), int(bb[1]-7)), cv2.FONT_HERSHEY_SIMPLEX , 0.4, colors[cat], 1, cv2.LINE_AA)
                        for ell in det["ellipses"]:
                            draw_ellipse(img, ell, color=colors[cat], thickness=1)

                if visualize:
                    cv2.imshow("fen", img)
                    cv2.waitKey(-1)
                if output_folder is not None:
                    name = ("frame_%04d_" % idx) + os.path.splitext(os.path.basename(f))[0]
                    cv2.imwrite(os.path.join(output_folder, name + ".png"), img)
            except:
                print("No results")
                pass


    

        total_iou = 0
        if pose is not None:
            for inlier in inliers:
                ell = detections[inlier[0][0]]["ellipses"][inlier[0][1]]
                ellipsoid = scene[inlier[1]]["ellipsoid"]
                ell_proj = ellipsoid.project(P)
                total_iou += compute_ellipses_iou(ell, ell_proj)
            total_ious[idx] = total_iou

    # Save the detections and predicted ellipses
    if output_file is not None:
        with open(output_file, "w") as fout:
            json.dump(output_data, fout)

    # Save errors files
    if output_errors and do_pose_computation:
        np.savetxt(os.path.join(output_errors, "rot_errors.txt"), orientation_errors)
        np.savetxt(os.path.join(output_errors, "pos_errors.txt"), position_errors)
        np.savetxt(os.path.join(output_errors, "times.txt"), times)
        np.savetxt(os.path.join(output_errors, "ref_times.txt"), ref_times)
        np.savetxt(os.path.join(output_errors, "nb_inliers.txt"), nb_inliers)
        np.savetxt(os.path.join(output_errors, "total_ious.txt"), total_ious)
        np.savetxt(os.path.join(output_errors, "nb_iterations.txt"), nb_iterations)
        np.savetxt(os.path.join(output_errors, "gt_positions.txt"), out_gt_positions)
        np.savetxt(os.path.join(output_errors, "est_positions.txt"), out_est_positions)
        save_obj(os.path.join(output_errors, "gt_positions.obj"), out_gt_positions)
        save_obj(os.path.join(output_errors, "est_positions.obj"), out_est_positions)
        print("Errors files saved in", output_errors)

    if output_file is not None:
        print("\nOutput predictions and poses saved in", output_file)



if __name__ == '__main__':
    import sys
    try:
        main(sys.argv[1:])
    except Exception as e:
        logging.exception(e)
        sys.exit(1)
