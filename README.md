# Level Set-Based Camera Pose Estimation

This repository contains the code for camera pose refinement by minimizing an ellipse-ellipse reprojection error. We propose our own ellipse-ellpise cost which has nice properties for this problem and compare it with other existing metrics. We also predict uncertainty corresponding to the objects elliptic detections and use it to weight the contribution of each object during the optimization.

## Related Publications
- **Level Set-Based Camera Pose Estimation From Multiple 2D/3D Ellipse-Ellipsoid Correspondences.** Matthieu Zins, Gilles Simon, Marie-Odile Berger, *International Conference on Intelligent Robots and Systems (IROS 2022).* [Paper](http://arxiv.org/abs/2207.07953) | [Video](https://youtu.be/TxtsvlMDxbM)



<p align="center">
<img src='doc/iros_teaser.png' alt="IROS Teaser"/>
</p>





<p align="center">
<img src='doc/ellipses_alignment.gif' alt="Different behaviours of the metrics"/>
</p>




## Installation

The file ```env/environment.yml``` lists the basic dependencies.
Run the following command to create a virtual environment (called *visual-loc*) with these dependencies installed.


```
conda env create -f env/environment.yml
```

### Pyellcv library

The code additionally depends on the [**pyellcv**](https://gitlab.inria.fr/tangram/pyellcv) library for ellipses/ellipsoids manipulation, pose computation and the implementation of metrics.

```
python -m pip install 'git+https://gitlab.inria.fr/tangram/pyellcv.git'
# (add --user if you don't have permission)

# Or, to install it from a local clone:
git clone --recursive https://gitlab.inria.fr/tangram/pyellcv.git
python -m pip install -e ./pyellcv
```

### Object Detection

We use Detectron2 for object detection. It provides implementation and pre-trained weights for state-of-the-art object detection algorithms. In particular, we use the Faster R-CNN architecture.

```
python -m pip install 'git+https://github.com/facebookresearch/detectron2.git'
# (add --user if you don't have permission)

# Or, to install it from a local clone:
git clone https://github.com/facebookresearch/detectron2.git
python -m pip install -e detectron2
```

### Ellipse prediction

We use a previous work [3D Aware Ellipse For Visual Localization](https://gitlab.inria.fr/tangram/3d-aware-ellipses-for-visual-localization) to obtain oriented ellipses per object.
The steps to train/test ellipse prediction are described in that repository. Here, the training has a new option `--use_uncertainty` to enable uncertainty as an additional output of the network.



## Test the method


### Dataset pre-processing

To demonstrate the method, we used the [Chess](http://download.microsoft.com/download/2/8/5/28564B23-0828-408F-8631-23B1EFF1DAC8/chess.zip) scene of the [7-Scenes](https://www.microsoft.com/en-us/research/project/rgb-d-dataset-7-scenes/) dataset.

<p align="center">
<img src='doc/7-Scenes_scene_model.png' alt="object-based-loc" height="300"/>
</p>

We provide:
 - a scene pre-built model for the Chess scene in `data/7-Scenes_Chess_scene.json`.
 - pre-generated files for objects 3D-aware elliptic detections in `data/7-Scenes_Chess_external_improved_detections.json` and `data/7-Scenes_Chess_external_improved_detections_with_uncertainty.json` (with predicted uncertainty).
 - a pre-processing script to generate the dataset JSON file and update the detection files with its location on your computer. Simply run `sh run_preprocessing.sh path/to/chess/scene/folder`.

### Pre-trained weights

Trained models object detection and ellipse prediction (with or without uncertainty) can be found at: [https://dorel.univ-lorraine.fr/dataset.xhtml?persistentId=doi:10.12763/N6LHZF](https://dorel.univ-lorraine.fr/dataset.xhtml?persistentId=doi:10.12763/N6LHZF)

|name| description |
|---|---|
|[Detectron](https://dorel.univ-lorraine.fr/api/access/datafile/:persistentId?persistentId=doi:10.12763/N6LHZF/XHPPLU)| pre-trained weights for object detection on the Chess scene. |
|[3D-aware ellipse prediction](https://dorel.univ-lorraine.fr/api/access/datafile/:persistentId?persistentId=doi:10.12763/N6LHZF/C65NAL)| pre-trained weights for ellipse prediction on the Chess scene. |
|[3D-aware ellipse prediction with uncertainty](https://dorel.univ-lorraine.fr/api/access/datafile/:persistentId?persistentId=doi:10.12763/N6LHZF/2E0IMK)| pre-trained weights for ellipse prediction with uncertainty on the Chess scene. |




## Run Pose Refinement

Run the following code to run th epose estimation algorithm:

```bash
python run.py $SCENE $DATASET \
 $DETECTRON_CHECKPOINT \
 $ELLIPSE_PREDICTION_CHECKPOINTS \
 --external_detections $DETECTIONS_FILE \
 --output_errors $OUTPUT_FOLDER \
 --output_images $OUTPUT_FOLDER \
 --skip_frames $NB_SKIP 
 --refine_pose \
 --cost $COST_MODE
 --solver $SOLVER_TYPE
 --use_uncertainty
```

Important options:
 - `$SCENE`: JSON file containing the scene model
 - `$DATASET`: JSON file of the dataset
 - `$DETECTRON_CHECKPOINT`: detectron weights
 - `$ELLIPSE_PREDICTION_CHECKPOINTS`: folder containing the ellipse prediction weights
 - `--external_detections $DETECTIONS_FILE`: use pre-computed objects elliptic detections from an external file. (disabling $DETECTRON_CHECKPOINT and $DETECTRON_CHECKPOINT)
 - `--refine-pose`: enable pose refinement
 - `--cost $COST_MODE`: sets the ellipse-ellipse cost used
 - `--solve $SOLVER_TYPE`: sets the method used for optimization
 - `--use_uncertainty`: enable to balance objects contribution using predicted uncertainty



**Available ellipse-ellipse metrics:**
|name| description   |
|---|---|
|`iou`| Intersection-over-Union |
|`giou`| Generalized Intersection-over-Union  |
|`bbox`| Bounding Boxes distance |
|`bbox_norm`| Bounding Boxes distance normalized |
|`qbbox`| In-image BBox distance from QuadricSLAM (C++ version) |
|`qbbox_py`| In-image BBox distance from QuadricSLAM (python version) |
|`alg_vec`| Algebraic vectorized distance |
|`alg_fro`| Algebraic Frobenius distance  |
|`alg_vec_norm`| Algebraic vectorized distance normalized |
|`alg_fro_norm`| Algebraic Frobenius distance normalized |
|`wass`| Wasserstein distance |
|`wass_norm`| Wasserstein distance normalized |
|`bhattacharyya`| Bhattacharyya distance |
|`dist_ctr`| Distance between contours |
|`dist_ctr_norm`| Distance between contours normalized |
|`kl_norm`| Kullback–Leibler divergence |
|`level_set`| Level Set-based distance (Ours) |


The normalization mentioned in some metrics is is used to treat equally pairs of large/small ellipses. It scales the pair of ellipses such that the largest axes of the detection ellipse is of length 1. Note that some other metrics are naturally normalized, such as *Level-Set*.




**Available solvers**
|name| description   |
|---|---|
|`bfgs`| Broyden–Fletcher–Goldfarb–Shanno algorithm (BFGS) |
|`lstsq`| Non-linear Least-Squares using the Trust Region Reflective algorithm |


In the paper, `minimize` has been used for all metrics for fairness in the comparison, even `lstsq` provide slightly better 
results and faster optimization for some metrics (Level-Set for example). For more details about optimization algorithms, see [Scipy documentation](https://docs.scipy.org/doc/scipy/tutorial/optimize.html).


<p align="center">
<img src='doc/iros_results.png' alt="IROS Results"/>
</p>




