# This file is part of Level-Set-Based-Camera-Pose-Estimation.
#
# Author: Matthieu Zins (matthieu.zins@inria.fr)
#
# Level-Set-Based-Camera-Pose-Estimation is free software: you can
# redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# Level-Set-Based-Camera-Pose-Estimation is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Level-Set-Based-Camera-Pose-Estimation.
# If not, see <http://www.gnu.org/licenses/>.


import copy
import json
import os
import cv2
import numpy as np
from PIL import Image
import torch

from detectron2 import model_zoo
from detectron2.config import get_cfg
from detectron2.engine import DefaultPredictor

from ellcv.types import Ellipse
from ellcv.utils import ellipse_from_bbox

from config.config import _cfg
from ellipse_prediction.model_uncertainty import EllipsePredictor
from utils.utils import force_square_bbox



class Improved_object_detector:
    """
        Improved object detector (with uncertainty) which combines bounding box prediction and
        3D-aware ellipse prediction.
    """

    def __init__(self, detector_checkpoint, ellipses_checkpoints_folder, enable_ellipses_prediction=True, mode="best"):
        # Configure Object detector
        self.cfg = get_cfg()
        self.cfg.merge_from_file(model_zoo.get_config_file(_cfg.DETECTOR_ARCHITECTURE))
        self.cfg.MODEL.ROI_HEADS.NUM_CLASSES = _cfg.DETECTOR_NUM_CLASSES
        self.cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = _cfg.DETECTOR_SCORE_THRESH_TEST
        self.cfg.MODEL.WEIGHTS = detector_checkpoint
        self.predictor = DefaultPredictor(self.cfg)

        # Configure Ellipse predictor
        self.device = torch.device("cuda")
        self.models = {}
        self.enable_ellipses_prediction = enable_ellipses_prediction
        if self.enable_ellipses_prediction:
            for folder in sorted(os.listdir(ellipses_checkpoints_folder)):
                try:
                    s = folder.split("_")
                    cat_id = int(s[1])
                    obj_id = int(s[2])
                    ckpt_file = os.path.join(ellipses_checkpoints_folder,
                                            "obj_%02d_%02d" % (cat_id, obj_id), "ckpt-" + mode + ".ckpt")
                    self.model = EllipsePredictor().to(self.device)
                    self.model.load(ckpt_file)
                    if cat_id in self.models.keys():
                        self.models[cat_id].append((obj_id, self.model))
                    else:
                        self.models[cat_id] = [(obj_id, self.model)]
                    print("Loaded model for object", cat_id, obj_id)
                except:
                    print("Impossible to load weights from", folder)


    def detect(self, img_filename):
        """
            Run object detection.
        """
        # Read image twice (cv2 and PIL) to ensure each network has the correct input format
        img = cv2.imread(img_filename)
        img_pil = Image.open(img_filename)

        # Run object detection
        predictions = self.predictor(img)
        instances = predictions["instances"]
        classes = instances.get("pred_classes").cpu().numpy().astype(int)
        scores = instances.get("scores").cpu().numpy()
        boxes = instances.get("pred_boxes").tensor.cpu().numpy()
        detections = []
        for c, s, b in zip(classes, scores, boxes):
            if s > self.cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST:
                x1, y1, x2, y2 = force_square_bbox(b, margin=2)
                pred_ellipses = []
                pred_uncertainties = []
                pred_obj_ids = []
                if self.enable_ellipses_prediction and c in self.models.keys():
                    # Run ellipse prediction
                    crop = img_pil.crop((x1, y1, x2, y2))
                    for (obj_id, model) in self.models[c]:
                        axes, angle, center, sigma2 = model.predict(crop, self.device)
                        center += np.array([x1, y1])
                        ellipse = Ellipse.compose(axes, angle, center)
                        pred_ellipses.append(ellipse)
                        pred_uncertainties.append(float(sigma2))
                        pred_obj_ids.append(obj_id)
                else:
                    # Fit ellipse directly to bbox
                    ellipse = ellipse_from_bbox(*b.flatten())
                    pred_ellipses.append(ellipse)
                    pred_uncertainties.append(0)

                detections.append({"category_id": int(c),
                                   "detection_score": float(s),
                                   "bbox": b.tolist(),
                                   "ellipses": pred_ellipses,
                                   "uncertainties": pred_uncertainties,
                                   "obj_ids": pred_obj_ids})
        return detections


class External_improved_object_detector:
    """
        Simulated improved object detector from an external file containing
        the detections.
    """
    def __init__(self, external_dections_file, enable_ellipses_prediction=True):
        try:
            with open(external_dections_file, "r") as fin:
                self.ext_detections = json.load(fin)
        except:
            print("Error: impossible to load detections from", external_dections_file)
        self.map_filename_index = {}
        for index, data in enumerate(self.ext_detections):
            self.map_filename_index[data["file_name"]] = index


    def detect(self, img_filename):
        if img_filename not in self.map_filename_index.keys():
            print("Warning: image not in the external detections file")
            return {}
        index = self.map_filename_index[img_filename]
        # Create the ellipse objects
        detections = self.ext_detections[index]["detections"]
        for det in detections:
            det["ellipses"] = [Ellipse.from_dict(ell_dict) for ell_dict in det["ellipses"]]
        return detections
