# This file is part of Level-Set-Based-Camera-Pose-Estimation.
#
# Author: Matthieu Zins (matthieu.zins@inria.fr)
#
# Level-Set-Based-Camera-Pose-Estimation is free software: you can
# redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# Level-Set-Based-Camera-Pose-Estimation is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Level-Set-Based-Camera-Pose-Estimation.
# If not, see <http://www.gnu.org/licenses/>.


import numpy as np

from ellcv.algo.cpp import solveP2E_ransac, solveP3P_ransac
from ellcv.types import Ellipsoid, Ellipse
from ellcv.visu import draw_ellipse, draw_bbox
import cv2

def compute_pose(detections, scene, K, min_obj_for_P3P=3, THRESHOLD=10, image=None):
    """
        Compute the camera pose using objects detections and a known scene model.
        Parameters:
            - detections: list of detections:
                [
                    {
                        "category_id": ...,
                        "bbox": [xmin, ymin, xmax, ymax],
                        "ellipses": [ Ellipse, Ellipse, Ellipse, ...]
                    }, ...
                ]
            - scene: scene loader
            - K: intrinsic matrix of the camera
            - min_obj_for_P3P: minimum number of objects needed to use P3P,
                               otherwise P2E can be used.
    """

    # Find mapping between detections and objects classes
    mapping_det_to_obj = [[] for i in range(len(detections))]
    for di, d in enumerate(detections):
        for oi, o in enumerate(scene):
            if o["category_id"] == d["category_id"]:
                mapping_det_to_obj[di].append(oi)

    if len(mapping_det_to_obj) < 2:
        print("Pose computation failed: Not enough objects can be used.")
        return None,  [], []


    ellipsoids_categories = [obj["category_id"] for obj in scene]
    ellipsoids_duals = [obj["ellipsoid"].as_dual() for obj in scene]

    detections_categories = [det["category_id"] for det in detections]
    detections_bboxes = [np.asarray(d["bbox"]).reshape((2, 2)) for d in detections]
    detections_ellipses_duals = [[ell.as_dual() for ell in pred["ellipses"]] for pred in detections]
    
        
    if len(detections_bboxes) >= min_obj_for_P3P:
        best_index, poses, scores, used_pairs, inliers, indiv_costs = solveP3P_ransac(
            ellipsoids_duals, ellipsoids_categories, detections_bboxes,
            detections_categories, detections_ellipses_duals, K, THRESHOLD)
    else:
        best_index, poses, scores, used_pairs, inliers = solveP2E_ransac(
            ellipsoids_duals, ellipsoids_categories, detections_bboxes,
            detections_categories, detections_ellipses_duals, K, THRESHOLD)

    if best_index < 0:
        print("Pose computation failed: No valid pose could be obtained.")
        return None, [], []

    return poses[best_index], used_pairs[best_index], inliers[best_index]
