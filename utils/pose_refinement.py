# This file is part of Level-Set-Based-Camera-Pose-Estimation.
#
# Author: Matthieu Zins (matthieu.zins@inria.fr)
#
# Level-Set-Based-Camera-Pose-Estimation is free software: you can
# redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# Level-Set-Based-Camera-Pose-Estimation is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Level-Set-Based-Camera-Pose-Estimation.
# If not, see <http://www.gnu.org/licenses/>.


import numpy as np
from ellcv.utils import ellipses_sampling_distance_cpp, ellipses_sampling_distance_ell_cpp, \
                        distance_to_ellipse_contour_cpp, ellipses_tangency_error_cpp, bbox_from_ellipse_cpp, \
                        ellipses_GW2_cpp, ellipses_Bhattacharyya_cpp, ellipses_sampling_distance_ell_uniform_cpp, \
                        ellipses_generalized_iou, find_on_image_bbox_cpp, find_on_image_bbox
from ellcv.types import Ellipse, Ellipsoid
from ellcv.utils import pose_error, sym2vec, compute_ellipses_iou
from ellcv.visu import draw_ellipse, draw_ellipse_plt, draw_bbox
from scipy.optimize import least_squares, minimize
from scipy.spatial.transform.rotation import Rotation as Rot
import cv2
import math
import seaborn as sns
import matplotlib.pyplot as plt
import sys


import warnings
warnings.simplefilter(action="ignore", category=FutureWarning)



def to_lie_algebra(R):
    return cv2.Rodrigues(R)[0]

def to_rotation_matrix(w):
    return cv2.Rodrigues(w)[0]
def kl_div(mu1, mu2, sigma1, sigma2):
    d = (mu1 - mu2)
    sigma2_inv = np.linalg.inv(sigma2)
    return 0.5 * (np.log(np.linalg.det(sigma2)/np.linalg.det(sigma1)) - 2 + d.dot(sigma2_inv.dot(d)) + np.trace(sigma2_inv @ sigma1))



def cost_least_squares(x, ellipsoids, Rt_init, K, fixed_ellipses, cost_mode, reduce, N_az, N_dist, confidence):
    R = Rot.from_euler("XYZ", x[:3]).as_matrix()

    t = x[3:]
    R_init, t_init = Rt_init
    R_tot = R @ R_init
    t_tot = R @ t_init + t

    P = K @ np.hstack((R_tot, t_tot.reshape((-1, 1))))
    proj = [e.project(P) for e in ellipsoids]
    
    errors = []
    for det, ell, conf in zip(fixed_ellipses, proj, confidence):
        axes, _, _ = det.decompose()
        scale = 1.0 / max(axes)
        if cost_mode == "iou":
            iou = compute_ellipses_iou(ell, det)
            values = 1.0 - iou
        elif cost_mode == "giou":
            giou =  ellipses_generalized_iou(ell, det)
            values = 1.0 - giou
        elif cost_mode == "bbox":
            bb1 = np.array(bbox_from_ellipse_cpp(det))
            bb2 = np.array(bbox_from_ellipse_cpp(ell))
            if reduce: values = np.linalg.norm(bb1 - bb2)**2
            else: values = bb1 - bb2
        elif cost_mode == "qbbox":
            status, bb1 = find_on_image_bbox_cpp(det, 640, 480)
            status, bb2 = find_on_image_bbox_cpp(ell, 640, 480)
            bb1 = np.asarray(bb1)
            bb2 = np.asarray(bb2)
            if reduce: values = np.linalg.norm(bb1 - bb2)**2
            else: values = bb1 - bb2
        elif cost_mode == "qbbox_py":
            status, bb1 = find_on_image_bbox(det, 640, 480)
            status, bb2 = find_on_image_bbox(ell, 640, 480)
            bb1 = np.asarray(bb1)
            bb2 = np.asarray(bb2)
            if reduce: values = np.linalg.norm(bb1 - bb2)**2
            else: values = bb1 - bb2
        elif cost_mode == "bbox_norm":
            bb1 = np.array(bbox_from_ellipse_cpp(det.full_scale(scale)))
            bb2 = np.array(bbox_from_ellipse_cpp(ell.full_scale(scale)))
            if reduce: values = np.linalg.norm(bb1 - bb2)**2
            else: values = bb1 - bb2
        elif cost_mode in ["alg_vec", "alg_fro"]:
            Q1 = det.full_scale(scale).as_dual()
            Q1 /= -Q1[2, 2]
            Q2 = ell.full_scale(scale).as_dual()
            Q2 /= -Q2[2, 2]
            if cost_mode == "alg_vec":
                if reduce:values = np.linalg.norm(sym2vec(Q1) - sym2vec(Q2))
                else: values = sym2vec(Q1) - sym2vec(Q2)
            elif cost_mode == "alg_fro":
                if reduce:values = np.linalg.norm(Q1 - Q2, ord="fro")
                else: values = (Q1 - Q2).flatten()
        elif cost_mode in ["alg_vec_norm", "alg_fro_norm"]:
            Q1 = det.full_scale(scale).as_dual()
            Q1 /= -Q1[2, 2]
            Q2 = ell.full_scale(scale).as_dual()
            Q2 /= -Q2[2, 2]
            if cost_mode == "alg_vec_norm":
                if reduce:values = np.linalg.norm(sym2vec(Q1) - sym2vec(Q2))
                else: values = sym2vec(Q1) - sym2vec(Q2)
            elif cost_mode == "alg_fro_norm":
                if reduce:values = np.linalg.norm(Q1 - Q2, ord="fro")
                else: values = (Q1 - Q2).flatten()
        elif cost_mode == "wass":
            values = ellipses_GW2_cpp(det, ell)
        elif cost_mode == "wass_norm":
            values = ellipses_GW2_cpp(det.full_scale(scale), ell.full_scale(scale))
        elif cost_mode == "bhattacharyya":
            values = ellipses_Bhattacharyya_cpp(det, ell)
        elif cost_mode == "level_set":
            N_az, N_dist = 6, 4
            sampling_scale = 1.0
            err, values, _, pts = ellipses_sampling_distance_ell_cpp(det, ell, N_az, N_dist, sampling_scale)
            if reduce:values = err
        elif cost_mode == "dist_ctr":
            err, values, _ = distance_to_ellipse_contour_cpp(ell, det, 50)
            if reduce:values = err
        elif cost_mode == "dist_ctr_norm":
            err, values, _ = distance_to_ellipse_contour_cpp(ell.full_scale(scale), det.full_scale(scale), 50)
            if reduce:values = err
        elif cost_mode == "kl_norm":
            mu_ell, sigma_ell = ell.full_scale(scale).as_gaussian()
            mu_det, sigma_det = det.full_scale(scale).as_gaussian()
            kl = kl_div(mu_ell, mu_det, sigma_ell, sigma_det)
            values = kl
        else:
            print("Warning unknown cost mode:", cost_mode)
            values = [0]
        errors.append(np.multiply(np.array(values), conf))


    if len(errors) == 0:
        if reduce: return 0
        else: return []

    if reduce:
        return np.sum(errors)
    else:
        return np.hstack(errors)



def sample_ellipses_values(ellipses, sampling_points, mode="[a b]"):
    """
        Sample values from ellipses at sampling points.
        Parameters:
            - ellipses: a list of ellipses
            - sampling_points: a list of sampling
        There should be the same number of ellipses and sampling points groups.
    """
    sampled_values = []
    for ell, pts in zip(ellipses, sampling_points):
        axes, angle, center = ell.decompose()
        R = np.array([[np.cos(angle), -np.sin(angle)],
                      [np.sin(angle), np.cos(angle)]])

        A = np.diag(np.reciprocal(axes**2))
        centered_pts = pts - center
        M = R @ A @ R.T @ centered_pts.T
        values = np.sqrt(np.einsum("ij,ji->i", centered_pts, M))
        sampled_values.append(values)
    return sampled_values


class PoseRefinement():
    def __init__(self, ellipses, ellipsoids, K, width, height, pose_init, cost_mode, bbox_padding=0, sampling_interval=2, solver="lstsq", pose_gt=None,
                 loss="linear", f_scale=1.0, uncertainties=None):
        self.ellipses = ellipses
        self.ellipsoids = ellipsoids
        self.K = K
        self.pose_init = pose_init
        self.Rt_init = [pose_init[0].T, -pose_init[0].T @ pose_init[1]]
        self.bbox_padding = bbox_padding
        self.sampling_interval = sampling_interval
        self.width = width
        self.height = height
        self.loss = loss
        self.f_scale = f_scale
        self.uncertainties = uncertainties

        self.cur_x = np.zeros(6)
        self.solver = solver
        self.pose_gt = pose_gt
        self.cost_mode = cost_mode
        self.sampled_ellipses_values = []
        self.sampling_points = []
        self.sampling_points_init = []
        self.good_pts = []
        self.global_bboxes = []
        self.global_scales = []

    def optimize(self, display=False, verbose=False, img=None, name=None, N_az=24, N_dist=6):
        confidence = np.ones(len(self.ellipses), dtype=float)
        if self.uncertainties is not None:
            confidence = np.reciprocal(self.uncertainties)
        nb_iter = 0
        if self.solver == "lstsq":
            res = least_squares(cost_least_squares, self.cur_x,
                                args=(self.ellipsoids, self.Rt_init, self.K, self.ellipses, self.cost_mode, False, N_az, N_dist, confidence),
                                method="trf", verbose=verbose,
                                loss=self.loss,
                                f_scale=self.f_scale)
            nb_iter = res.nfev
        elif self.solver == "bfgs":
            res = minimize(cost_least_squares, self.cur_x,
                                args=(self.ellipsoids, self.Rt_init, self.K, self.ellipses, self.cost_mode, True, N_az, N_dist, confidence))
            nb_iter = res.nit
        else:
            print("Invalid solver")
        self.cur_x = res.x
        final_o, final_p = self.get_final_pose()
        return final_o, final_p, nb_iter

    def get_final_pose(self):
        R_rel_est = Rot.from_euler("XYZ", self.cur_x[:3]).as_matrix()
        t_rel_est = self.cur_x[3:]
        R_tot = R_rel_est @ self.Rt_init[0]
        t_tot = R_rel_est @ self.Rt_init[1] + t_rel_est
        return R_tot.T, -R_tot.T @ t_tot
