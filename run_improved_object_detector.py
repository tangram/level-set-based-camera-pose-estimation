# This file is part of Level-Set-Based-Camera-Pose-Estimation.
#
# Author: Matthieu Zins (matthieu.zins@inria.fr)
#
# Level-Set-Based-Camera-Pose-Estimation is free software: you can
# redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# Level-Set-Based-Camera-Pose-Estimation is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Level-Set-Based-Camera-Pose-Estimation.
# If not, see <http://www.gnu.org/licenses/>.


import argparse
import logging
import json
import os

import cv2
import numpy as np

from ellcv.visu import draw_ellipse, draw_bbox

from config.config import _cfg
from dataset.dataset_loader import Dataset_loader
from object_detector import Improved_object_detector
from object_detector_with_uncertainty import Improved_object_detector as Improved_object_detector_uncertainty
from utils.utils import create_if_needed


def to_json_serializable(detections):
    return [{"category_id": d["category_id"],
             "detection_score": d["detection_score"],
             "bbox": d["bbox"],
             "ellipses" : [e.to_dict() for e in d["ellipses"]]
            } for d in detections]


def main(args):
    parser = argparse.ArgumentParser()
    parser.add_argument("dataset", help="<Required> Dataset to process (.json).")
    parser.add_argument("detector_checkpoint", help="<Required> Checkpoint for the detection network.")
    parser.add_argument("ellipses_checkpoints", help="<Required> Folder containing the checkpoints for ellipse predictions")
    parser.add_argument("output_file", help="<Required> Output file containing the detections (.json).")

    parser.add_argument("--mode", default="best",
                        help="Choose between 'best'(default) or 'last' checkpoint for ellipse prediction")
    parser.add_argument("--disable_ellipses_prediction",  action="store_true", default=False,
                        help="<Optional> Disable ellipses prediction and run only object detection (default is False).")
    parser.add_argument("--output_images", default=None,
                        help="<Optional> Output folder where to write the images with predictions and reporjected objects (default is None).")
    parser.add_argument("--visualize",  action="store_true", default=False,
                        help="<Optional> Visualize each predictions and estimated poses online (default is False)")
    parser.add_argument("--use_uncertainty",  action="store_true", default=False,
                        help="<Optional> Enable usage of uncertainty (default is False).")
    args = parser.parse_args(args)



    dataset_file = args.dataset
    detector_checkpoint = args.detector_checkpoint
    checkpoints_folder_ellipses = args.ellipses_checkpoints
    mode = args.mode
    output_file = args.output_file
    enable_ellipses_prediction = not args.disable_ellipses_prediction
    output_folder = args.output_images
    visualize = args.visualize
    use_uncertainty = args.use_uncertainty

    if output_folder is not None:
        create_if_needed(output_folder)

    # Load the dataset
    loader = Dataset_loader(dataset_file)

    # Create the detector
    if use_uncertainty:
        detector = Improved_object_detector_uncertainty(detector_checkpoint, checkpoints_folder_ellipses,
                                                        enable_ellipses_prediction=enable_ellipses_prediction, mode=mode)
    else:
        detector = Improved_object_detector(detector_checkpoint, checkpoints_folder_ellipses,
                                            enable_ellipses_prediction=enable_ellipses_prediction, mode=mode)


    # Processing loop
    output_data = []
    position_errors = [-1] * len(loader)
    orientation_errors = [-1] * len(loader)
    for idx in range(len(loader)):
        f = loader.get_rgb_filename(idx)

        # Run detector
        detections = detector.detect(f)

        out_data = {"file_name": f, 
                    "detections": to_json_serializable(detections),
                    "orientation": [],
                    "position": []}

        output_data.append(out_data)
        
        print("({:4} / {}) => {} detected objects".format(idx, len(loader), len(detections)))

        if visualize or output_folder is not None:
            img = cv2.imread(f)
            # Display only detections and predicted ellipses
            colors = {}
            for det_i, det in enumerate(detections):
                bb = np.round(det["bbox"]).astype(int)
                cat = det["category_id"]
                if cat not in colors.keys():
                    colors[cat] = np.random.uniform(0, 255, size=(3))
                draw_bbox(img, bb, color=colors[cat], thickness=2)
                cv2.putText(img, "%.2f(%d)" % (det["detection_score"], det["category_id"]), (int(bb[0]+10), int(bb[1]-7)), cv2.FONT_HERSHEY_SIMPLEX , 0.4, colors[cat], 1, cv2.LINE_AA)
                if use_uncertainty:
                    print("Object %d(%d) uncertainties: " % (det_i, cat), *det["uncertainties"])
                for ell in det["ellipses"]:
                    draw_ellipse(img, ell, color=colors[cat], thickness=1)

            if visualize:
                cv2.imshow("fen", img)
                cv2.waitKey(-1)
            if output_folder is not None:
                name = ("frame_%04d_" % idx) + os.path.splitext(os.path.basename(f))[0]
                cv2.imwrite(os.path.join(output_folder, name + ".png"), img)

    # Save the detections and predicted ellipses
    if output_file is not None:
        with open(output_file, "w") as fout:
            json.dump(output_data, fout)

    if output_file is not None:
        print("\nOutput predictions and poses saved in", output_file)



if __name__ == '__main__':
    import sys
    try:
        main(sys.argv[1:])
    except Exception as e:
        logging.exception(e)
        sys.exit(1)