# This file is part of Level-Set-Based-Camera-Pose-Estimation.
#
# Author: Matthieu Zins (matthieu.zins@inria.fr)
#
# Level-Set-Based-Camera-Pose-Estimation is free software: you can
# redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# Level-Set-Based-Camera-Pose-Estimation is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Level-Set-Based-Camera-Pose-Estimation.
# If not, see <http://www.gnu.org/licenses/>.


import argparse
import json
import logging
import os

"""
    Update the folder containing the Chess image sequences for the precomputed object detections provided.
"""



def main(args):
    parser = argparse.ArgumentParser()
    parser.add_argument("chess_folder", help="<Required> 7-Scenes Chess folder.")
    args = parser.parse_args(args)

    chess_folder = args.chess_folder

    detection_files = ["data/7-Scenes_Chess_external_improved_detections.json",
                       "data/7-Scenes_Chess_external_improved_detections_with_uncertainty.json"]
    for det_file in detection_files:
        with open(det_file, "r") as fin:
            data = json.load(fin)
        for d in data:
            f = d["file_name"]
            new_f = os.path.join(chess_folder, "/".join(f.split("/")[-2:]))
            d["file_name"] = new_f

        with open(det_file, "w") as fout:
            json.dump(data, fout)

        print(det_file, "has been updated.")


if __name__ == '__main__':
    import sys
    try:
        main(sys.argv[1:])
    except Exception as e:
        logging.exception(e)
        sys.exit(1)